# symfony/security-core

Core Security Library. https://symfony.com/doc/current/components/security

## Voters
* [*How to Use Voters to Check User Permissions*
  ](https://symfony.com/doc/current/security/voters.html)
  Symfony
* [*Grant on Permissions, not Roles*
  ](https://wouterj.nl/2020/01/grant-on-permissions-not-roles)
  2020-01 Wouter J

## Unofficial documentation
* [*How to login a user in unit test within Symfony in an example*
  ](https://medium.com/@youssefbenhssaien/how-to-connect-a-user-in-unit-test-within-symfony-in-an-example-bcbf5ab4a86a)
  2020-07 Youssef Benhssaien
* [*Improve your voters with the Open/Closed principle:
    Think about provided examples and improve them with some OOP concepts*
  ](https://medium.com/darkmirafr/improve-your-voters-with-the-open-closed-principle-1eae3f77f505)
  2020-06 Vincent Monjaret
* [*Basic Authentication and Registration Steps with Symfony Security Bundle (Symfony 5)*
  ](https://medium.com/suleyman-aydoslu/basic-authentication-and-registration-steps-with-symfony-security-bundle-symfony-5-a5518ee0a9da)
  2020-05 Süleyman Aydoslu
* [Symfony 4.4 Security Cheat Sheet
  ](https://medium.com/@andreiabohner/symfony-4-4-security-cheat-sheet-5e9e75fbacc0)
  2019-06 Andréia Bohner
  * [Powerful and complete Security for your apps! Symfony 4.4
    ](http://assets.andreiabohner.org/symfony/sf44-security-cheat-sheet.pdf)
* fr [*Récupérer plus simplement l’utilisateur dans un contrôleur Symfony*
  ](https://blog.netinfluence.ch/2019/04/12/recuperer-plus-simplement-lutilisateur-dans-un-controleur-symfony/)
  2019-04 [Romaric](https://blog.netinfluence.ch/author/romaric/)
* [*Understanding Symfony Security by Using it Standalone*](https://wouterj.nl/2019/03/understanding-symfony-security-by-using-it-standalone)
  2019-03 Wouter de Jong
* [*Decoupling your application User entity from Symfony’s security*](https://medium.com/@simshaun/decoupling-your-application-user-from-symfonys-security-user-60fa31b4f7f2)
  2018-11 Shaun Simmons
  * Uses also security-guard
### (fr)
* [*Création d’un système d’authentification (login & registration) en utilisant Symfony 6*
  ](https://medium.com/@naamen.mohamed_2146/cr%C3%A9ation-dun-syst%C3%A8me-d-authentification-login-registration-en-utilisant-symfony-6-191f8bc84e3a)
  2023-01 Naamen Mohamed Amine
